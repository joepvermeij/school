﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Rekenmachbine
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Maal_Click(object sender, RoutedEventArgs e)
        {
            string getal1 = Getal_1.Text;
            string getal2 = Getal_2.Text;
            double getal1_waarde = Convert.ToDouble(getal1);
            double getal2_waarde = Convert.ToDouble(getal2);
            double Resultaat_maal = getal1_waarde * getal2_waarde;
            Resultaat.Content = Resultaat_maal;
        }

        private void Gedeeld_Click(object sender, RoutedEventArgs e)
        {
            string getal1 = Getal_1.Text;
            string getal2 = Getal_2.Text;
            double getal1_waarde = Convert.ToDouble(getal1);
            double getal2_waarde = Convert.ToDouble(getal2);
            double Resultaat_maal = getal1_waarde / getal2_waarde;
            Resultaat.Content = Resultaat_maal;
        }

        private void Plus_Click(object sender, RoutedEventArgs e)
        {
            string getal1 = Getal_1.Text;
            string getal2 = Getal_2.Text;
            double getal1_waarde = Convert.ToDouble(getal1);
            double getal2_waarde = Convert.ToDouble(getal2);
            double Resultaat_maal = getal1_waarde + getal2_waarde;
            Resultaat.Content = Resultaat_maal;
        }

        private void Min_Click(object sender, RoutedEventArgs e)
        {
            string getal1 = Getal_1.Text;
            string getal2 = Getal_2.Text;
            double getal1_waarde = Convert.ToDouble(getal1);
            double getal2_waarde = Convert.ToDouble(getal2);
            double Resultaat_maal = getal1_waarde - getal2_waarde;
            Resultaat.Content = Resultaat_maal;
        }

        private void wissen_Click(object sender, RoutedEventArgs e)
        {
            Resultaat.Content = "";
            Getal_1.Text = "";
            Getal_2.Text = "";
        }

        private void Getal_1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string getal1 = Getal_1.Text;
                string getal2 = Getal_2.Text;
                double getal1_waarde = Convert.ToDouble(getal1);
                double getal2_waarde = Convert.ToDouble(getal2);
                double Resultaat_maal = getal1_waarde + getal2_waarde;
                Resultaat.Content = Resultaat_maal;
            }
        }
    }
}
