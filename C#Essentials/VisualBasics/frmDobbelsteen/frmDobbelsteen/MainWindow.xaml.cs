﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace frmDobbelsteen
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int count;
        private Random random;
        private StringBuilder tekstBouwer;
        public MainWindow()
        {
            InitializeComponent();
            random = new Random();
            count = 0;
            tekstBouwer = new StringBuilder();

        }

        private void ResetVenster()
        {
            DobbelsteenRestultaat.Text = "";
        }
        private void Start_Click(object sender, RoutedEventArgs e)
        {
            int Dobbelsteen;
            do
            {
                Dobbelsteen = random.Next(1, 7);
                count++;
                tekstBouwer.AppendLine($"Worp {count} geeft {Dobbelsteen}");
            } while (Dobbelsteen !=6);
            DobbelsteenRestultaat.Text = tekstBouwer.ToString();
        }

        private void Opnieuw_Click(object sender, RoutedEventArgs e)
        {
            ResetVenster();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
