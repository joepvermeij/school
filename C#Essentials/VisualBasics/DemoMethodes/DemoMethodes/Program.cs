﻿using System;

namespace DemoMethodes
{
    class Program
    {
        static void Main(string[] args)
        {
            //Groet("Sander");
            int cijfer = 2;
            Console.WriteLine($"Dit is cijfer: {cijfer}");
            PlusEen(ref cijfer);
            Console.WriteLine($"Dit is cijfer: {cijfer}");
        }
        private static void Groet(string naam)
        {
            Console.WriteLine("Dag" + naam);
        }

        private static void PlusEen(ref int getal)
        {
            Console.WriteLine("start methode");
            getal = getal + 1;            
            Console.WriteLine(getal);
            Console.WriteLine("einde methode");
        }


    }
}
