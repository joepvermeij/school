﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DemoWPFMethodes
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DemoTextBlock.Text = "Dit is aangepast in de constructor";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine();
            SetTextBlockNumber(314);
        }

        private void SetTextBlockNumber(int getal) 
        {
            DemoTextBlock.Text = Convert.ToString(getal);
        }

        private int DoubleNumber(int getal)
        {
            int dubbel = getal * 2;
            return dubbel;
            // return getal * 2;
        }
    }
}
