﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Microsoft.VisualBasic;

namespace GalgjeWerkpleklerenProjectWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string antwoord;
        private string juisteLetters;
        private string fouteLetters;
        private int levens;
        private string guess;
        private char[] antwoordInLetters;
        private char[] geradenLetters;
        private List<char> alGeradenLetters;
        private DispatcherTimer timer;
        private int tijd;
        private int bepaaldeTijd;
        private char charGuess;
        private StringBuilder highScores;
        private string playerNaam;
        private bool isHintGevraagd;
        private string[] galgjeWoorden;
        private BitmapImage balonnenMannetje;
        private BitmapImage GalgjeMannetje;
        private bool afbeeldingVeranderen = true;


        public MainWindow()
        {
            InitializeComponent();
            StartSpel();
            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Tick += Timer_Tick;
            highScores = new StringBuilder();
            galgjeWoorden = new string[]{
                "grafeem",
                "tjiftjaf",
                "maquette",
                "kitsch",
                "pochet",
                "convocaat",
                "jakkeren",
                "collaps",
                "zuivel",
                "cesium",
                "voyant",
                "spitten",
                "pancake",
                "gietlepel",
                "karwats",
                "dehydreren",
                "viswijf",
                "flater",
                "cretonne",
                "sennhut",
                "tichel",
                "wijten",
                "cadeau",
                "trotyl",
                "chopper",
                "pielen",
                "vigeren",
                "vrijuit",
                "dimorf",
                "kolchoz",
                "janhen",
                "plexus",
                "borium",
                "ontweien",
                "quiche",
                "ijverig",
                "mecenaat",
                "falset",
                "telexen",
                "hieruit",
                "femelaar",
                "cohesie",
                "exogeen",
                "plebejer",
                "opbouw",
                "zodiak",
                "volder",
                "vrezen",
                "convex",
                "verzenden",
                "ijstijd",
                "fetisj",
                "gerekt",
                "necrose",
                "conclaaf",
                "clipper",
                "poppetjes",
                "looikuip",
                "hinten",
                "inbreng",
                "arbitraal",
                "dewijl",
                "kapzaag",
                "welletjes",
                "bissen",
                "catgut",
                "oxymoron",
                "heerschaar",
                "ureter",
                "kijkbuis",
                "dryade",
                "grofweg",
                "laudanum",
                "excitatie",
                "revolte",
                "heugel",
                "geroerd",
                "hierbij",
                "glazig",
                "pussen",
                "liquide",
                "aquarium",
                "formol",
                "kwelder",
                "zwager",
                "vuldop",
                "halfaap",
                "hansop",
                "windvaan",
                "bewogen",
                "vulstuk",
                "efemeer",
                "decisief",
                "omslag",
                "prairie",
                "schuit",
                "weivlies",
                "ontzeggen",
                "schijn",
                "sousafoon"
            };
        }
        // Verbergt Het woord dat player 1 heeft opgegeven, indien niks opgegeven kiest het een woord uit galgjewoorden.
        private void Verberg_Click(object sender, RoutedEventArgs e)
        {
            if (RaadText.Text == "")
            {
                Random rnd = new Random();
                int galgjeWoordenIndex = rnd.Next(galgjeWoorden.Length);
                antwoord = galgjeWoorden[galgjeWoordenIndex];
            }
            else
            {
                antwoord = RaadText.Text;
            }
            Raad.Visibility = Visibility.Visible;
            Verberg.Visibility = Visibility.Hidden;
            TimerInstellen.Visibility = Visibility.Collapsed;
            antwoordInLetters = antwoord.ToCharArray();
            geradenLetters = new char[antwoord.Length];
            for (int i = 0; i < geradenLetters.Length; i++)
            {
                geradenLetters[i] = '*';
            }
            RaadText.Text = "";
            tijd = bepaaldeTijd + 1;
            timer.Start();
            Resultaat.Text = $"{levens} Levens\nJuiste Letters: {juisteLetters}\nFoute Letters: {fouteLetters}\n {string.Join("", geradenLetters)}";
            isHintGevraagd = false;
        }
        // Als je op raad drukt dan afhandelijk van of je 1 letter hebt opgegeven of een woord  gaat het kijken of het overeenkomt
        private void Raad_Click(object sender, RoutedEventArgs e)
        {
            guess = RaadText.Text;
            RaadText.Text = "";
            if (guess.Length != 1)
            {
                char[] geradenWoord;
                geradenWoord = guess.ToCharArray();
                if (guess == antwoord)
                {
                    AntwoordGeraden(geradenWoord);
                }
                else
                {
                    FoutGeraden();
                }
            }
            else if(IsLetterAlGeraden())
            {
                Resultaat.Text = $"De letter '{guess}' is al geraden";
            }
            else if (antwoord.Contains(guess))
            {
                charGuess = char.Parse(guess);
                juisteLetters = juisteLetters + guess;
                for (int i = 0; i < antwoord.Length; i++)
                {
                    if (charGuess == antwoordInLetters[i])
                    {
                        geradenLetters[i] = antwoordInLetters[i];
                    }
                }
                AntwoordGeraden(geradenLetters);
            }
            else
            {
                fouteLetters = fouteLetters + guess;
                FoutGeraden();
            }
            tijd = bepaaldeTijd + 1;
        }
        // start een nieuw spel
        private void NieuwSpel_Click(object sender, RoutedEventArgs e)
        {
            StartSpel();
            timer.Stop();
        }
        // start de TImer en reset het als het op nul en start foutgeraden method.
        private void Timer_Tick(object sender, EventArgs e)
        {
            tijd--;
            if (tijd == 0)
            {
                FoutGeraden();
                GridBackground.Background = Brushes.Red;
                TimerText.Text = "Tijd: 0";
            }
            else if (tijd == bepaaldeTijd + 1)
            {
                TimerText.Text = "Tijd: " + bepaaldeTijd.ToString();
            }
            else
            {
                GridBackground.Background = Brushes.CadetBlue;
                TimerText.Text = "Tijd: " + tijd.ToString();
            }
        }
        // Sluit het programma
        private void Afsluiten_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        // laat de highscores zien
        private void Highscores_Click(object sender, RoutedEventArgs e)
        {
            Resultaat.Text = highScores.ToString();
        }
        // Maakt het mogelijk om een Hint te vragen
        private void HintVragen_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult wilJeEenHint = MessageBox.Show("Ben je zeker? Je krijgt geen Highscore.", "Hint opvragen", MessageBoxButton.YesNo);
            if (wilJeEenHint == MessageBoxResult.Yes)
            {
                char myRandomLowerCase;
                do
                {
                    Random rnd2 = new Random();
                    int ascii_index2 = rnd2.Next(97, 123);
                    myRandomLowerCase = Convert.ToChar(ascii_index2);
                } while (antwoord.Contains(myRandomLowerCase));
                MessageBox.Show($"Dit woordt bevat niet de letter: '{myRandomLowerCase}'");
                isHintGevraagd = true;
            }
        }
        // laat je de timer veranderen
        private void TimerInstellen_Click(object sender, RoutedEventArgs e)
        {
            while (
                !int.TryParse(Interaction.InputBox("Geef een nieuwe Timer. (Tussen 5 en 20 seconden)"), out bepaaldeTijd) || 
                bepaaldeTijd < 5 || 
                bepaaldeTijd > 20) ;

        }
        // Afbeelding Veranderen(galgje of belonnen)
        private void AfbeeldingVeranderen_Check(object sender, RoutedEventArgs e)
        {
            afbeeldingVeranderen = true;
            ImageSelect();
        }
        // Afbeelding Veranderen(galgje of belonnen)
        private void AfbeeldingVeranderen_UnCheck(object sender, RoutedEventArgs e)
        {
            afbeeldingVeranderen = false;
            ImageSelect();
        }
        // Checkt of het antwoord geraden is, indien ja stopt het spel anders gebeurt er niks
        private void AntwoordGeraden(char[] komtOvereen)
        {
            if (IsArrayHetzelfde(komtOvereen, antwoordInLetters))
            {
                Resultaat.Text = $"Hoera!!!\n Je hebt \n \"{antwoord}\" \n correct geraden!";
                Raad.Visibility = Visibility.Hidden;
                timer.Stop();
                if (isHintGevraagd == false)
                {
                    highScores.AppendLine($"{playerNaam} - {levens} levens ({DateTime.Now.ToLongTimeString()})\n");
                }
            }
            else
            {
                Resultaat.Text = $"{levens} Levens\nJuiste Letters: {juisteLetters}\nFoute Letters: {fouteLetters}\n {string.Join("", geradenLetters)}";
            }
        }
        // als er een Fout Geraden is of de timer op nul gaat start deze methode. Dit vermindert de levens en checkt of de levens op nul staat en reset timer.
        private void FoutGeraden()
        {
            levens--;
            if (levens == 0)
            {
                Resultaat.Text = $"Je hebt het geheim\n niet op tijd geraden.\nJe bent opgehangen";
                Raad.Visibility = Visibility.Hidden;
                timer.Stop();
            }
            else
            {
                Resultaat.Text = $"{levens} Levens\nJuiste Letters: {juisteLetters}\nFoute Letters: {fouteLetters}\n {string.Join("", geradenLetters)}";
            }
            tijd = bepaaldeTijd + 1;
            ImageSelect();

        }
        // Alle Code die nodig is om een nieuw spel te starten cleart de velden reset levens etc.
        private void StartSpel()
        {
            antwoord = "";
            juisteLetters = "";
            fouteLetters = "";
            levens = 10;
            ImageSelect();
            guess = "";
            Raad.Visibility = Visibility.Hidden;
            Verberg.Visibility = Visibility.Visible;
            TimerInstellen.Visibility = Visibility.Visible;
            RaadText.Text = "";
            Resultaat.Text = "";
            alGeradenLetters = new List<char>();
            playerNaam = Interaction.InputBox("Geef u naam?");
            bepaaldeTijd = 10;
            Resultaat.Text = $"Speler Kan een woord\n ingeven indien gewenst\n of geef niks in en er wordt\n een woord gegenereerd.";
        }
        // Select de image op basis van het aantal levens
        private void ImageSelect()
        {
            if (afbeeldingVeranderen)
            {
                switch (levens)
                {
                    case 0:
                        GalgjeImage.Source = new BitmapImage(new Uri(@"/GalgjeImages/Galgje 11.png", UriKind.Relative));
                        break;
                    case 1:
                        GalgjeImage.Source = new BitmapImage(new Uri(@$"/GalgjeImages/Galgje 10.png", UriKind.Relative));
                        break;
                    case 2:
                        GalgjeImage.Source = new BitmapImage(new Uri(@"/GalgjeImages/Galgje 9.png", UriKind.Relative));
                        break;
                    case 3:
                        GalgjeImage.Source = new BitmapImage(new Uri(@"/GalgjeImages/Galgje 8.png", UriKind.Relative));
                        break;
                    case 4:
                        GalgjeImage.Source = new BitmapImage(new Uri(@"/GalgjeImages/Galgje 7.png", UriKind.Relative));
                        break;
                    case 5:
                        GalgjeImage.Source = new BitmapImage(new Uri(@"/GalgjeImages/Galgje 6.png", UriKind.Relative));
                        break;
                    case 6:
                        GalgjeImage.Source = new BitmapImage(new Uri(@"/GalgjeImages/Galgje 5.png", UriKind.Relative));
                        break;
                    case 7:
                        GalgjeImage.Source = new BitmapImage(new Uri(@"/GalgjeImages/Galgje 4.png", UriKind.Relative));
                        break;
                    case 8:
                        GalgjeImage.Source = new BitmapImage(new Uri(@"/GalgjeImages/Galgje 3.png", UriKind.Relative));
                        break;
                    case 9:
                        GalgjeImage.Source = new BitmapImage(new Uri(@"/GalgjeImages/Galgje 2.png", UriKind.Relative));
                        break;
                    default:
                        GalgjeImage.Source = new BitmapImage(new Uri(@"/GalgjeWerkpleklerenProjectWPF;component/GalgjeImages/Galgje 1.png", UriKind.Relative));
                        break;
                }
            }
            else
            {
                switch (levens)
                {
                    case 0:
                        GalgjeImage.Source = new BitmapImage(new Uri(@"/hangman/10.jpg", UriKind.Relative));
                        break;
                    case 1:
                        GalgjeImage.Source = new BitmapImage(new Uri(@"/hangman/9.jpg", UriKind.Relative));
                        break;
                    case 2:
                        GalgjeImage.Source = new BitmapImage(new Uri(@"/hangman/8.jpg", UriKind.Relative));
                        break;
                    case 3:
                        GalgjeImage.Source = new BitmapImage(new Uri(@"/hangman/7.jpg", UriKind.Relative));
                        break;
                    case 4:
                        GalgjeImage.Source = new BitmapImage(new Uri(@"/hangman/6.jpg", UriKind.Relative));
                        break;
                    case 5:
                        GalgjeImage.Source = new BitmapImage(new Uri(@"/hangman/5.jpg", UriKind.Relative));
                        break;
                    case 6:
                        GalgjeImage.Source = new BitmapImage(new Uri(@"/hangman/4.jpg", UriKind.Relative));
                        break;
                    case 7:
                        GalgjeImage.Source = new BitmapImage(new Uri(@"/hangman/3.jpg", UriKind.Relative));
                        break;
                    case 8:
                        GalgjeImage.Source = new BitmapImage(new Uri(@"/hangman/2.jpg", UriKind.Relative));
                        break;
                    case 9:
                        GalgjeImage.Source = new BitmapImage(new Uri(@"/hangman/1.jpg", UriKind.Relative));
                        break;
                    default:
                        GalgjeImage.Source = new BitmapImage(new Uri(@"/hangman/0.jpg", UriKind.Relative));
                        break;
                }
            }
            
        }
        // Checkt of de letter al geraden is
        private bool IsLetterAlGeraden()
        {
            charGuess = char.Parse(guess);
            if (alGeradenLetters.Contains(charGuess))
            {
                return true;
            }
            else
            {
                alGeradenLetters.Add(charGuess);
                return false;
            }
        }
        // Checkt of 2 arrays hetzelfde zijn.
        private bool IsArrayHetzelfde(Char[] array1, char[] array2)
        {
            for (int i = 0; i < antwoord.Length; i++)
            {
                if (array1[i] != array2[i])
                {
                    return false;
                }
            }
            return true;
        }

        
    }
}
