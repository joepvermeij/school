﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace EeveeCatcherWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DispatcherTimer klokTimer;
        private DispatcherTimer randomImageTimer;
        private Random random;
        private Dictionary<string, string> eeveeImages = new Dictionary<string, string> 
        { 
            { "Eevee", "/images/Eevee.png" }, 
            { "EeveeGigantamax", "/images/EeveeGigantamax.png" }, 
            { "Espeon", "/images/Espeon.png" }, 
            { "Flareon", "/images/Flareon.png" }, 
            { "Glaceon", "/images/Glaceon.png" }, 
            { "Jolteon", "/images/Jolteon.png" }, 
            { "Leafeon", "/images/Leafeon.png" }, 
            { "Sylveon", "/images/Sylveon.png" }, 
            { "Umbreon", "/images/Umbreon.png" }, 
            { "Vaporeon", "/images/Vaporeon.png" } 
        };
        private string huidigeAfdbeelding;

        public MainWindow()
        {
            InitializeComponent();
            StartKlok();
            random = new Random();
        }

        private void StartKlok()
        {
            klokTimer = new DispatcherTimer();
            klokTimer.Interval = TimeSpan.FromMilliseconds(1);
            klokTimer.Tick += KlokTimer_Tick;
            klokTimer.Start();
        }

        private void KlokTimer_Tick(object sender, EventArgs e)
        {
            TijdTextBlock.Text = DateTime.Now.ToLongTimeString();
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            StartButton.IsEnabled = false;
            Startspel();
        }

        private void Startspel()
        {
            randomImageTimer = new DispatcherTimer();
            randomImageTimer.Interval = new TimeSpan(0, 0, 0, 1);
            randomImageTimer.Tick += randomImageTimer_Tick;
            randomImageTimer.Start();
        }

        private void randomImageTimer_Tick(object sender, EventArgs e)
        {
            List<string> keys = new List<string>(eeveeImages.Keys);
            huidigeAfdbeelding = keys[random.Next(keys.Count)];
            RandomImage.Source = new BitmapImage(
                                    new Uri(eeveeImages[huidigeAfdbeelding], UriKind.Relative));
        }

        private void RandomImage_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (huidigeAfdbeelding.Equals("Eevee"))
            {
                MessageBox.Show("Jij wint");
                DateTime momentDatEeveeToont = DateTime.Now;
                DateTime nu = DateTime.Now;
                TimeSpan reactieSnelheid = nu - momentDatEeveeToont; 
            }
        }
    }
}
