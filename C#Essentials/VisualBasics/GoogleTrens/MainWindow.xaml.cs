﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GoogleTrens
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string[,] news;
        private string[,] games;
        private string[,] populaireTermen;
        private string[,] belgischePersonen;
        private Dictionary<string, string[,]> trendsDictionary;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Vul2DArrays();
            VulDictionary();
            VulComboBox();
            Top3RadioButton.IsChecked = true;
        }

        private void VulComboBox()
        {
            foreach (string key in trendsDictionary.Keys)
            {
                ComboBoxItem item = new ComboBoxItem();
                item.Content = key;
                TrendsComboBox.Items.Add(item);
            }
        }

        private void VulDictionary()
        {
            trendsDictionary = new Dictionary<string, string[,]>();
            trendsDictionary.Add("news", news);
            trendsDictionary.Add("games", games);
            trendsDictionary.Add("populaire termen", populaireTermen);
            trendsDictionary.Add("belgische personen", belgischePersonen);
        }

        private void Vul2DArrays()
        {
            games = new string[,]
            {
{ "1", "Among Us" },
{ "2", "Battlefield 2042" },
{ "3", "Resident Evil Village" },
{ "4", "Valheim" },
{ "5", "Forza Horizon 5" },
{ "6", "Madden NFL 22" },
{ "7", "Outriders" },
{ "8", "Pokémon Unite" },
{ "9", "Biomutant" },
{ "10", "Friday Night Funkin" }
            };
            news = new string[,]
            {
{ "1", "Mega Millions" },
{ "2", "AMC Stock" },
{ "3", "Stimulus Check" },
{ "4", "Georgia Senate Race" },
{ "5", "GME" },
{ "6", "Dogecoin" },
{ "7", "Hurricane Ida" },
{ "8", "Kyle Rittenhouse Verdict" },
{ "9", "Afghanistan" },
{ "10", "Ethereum Price" }
            };
            populaireTermen = new string[,]
            {
{ "1", "NBA" },
{ "2", "DMX" },
{ "3", "Gabby Petito" },
{ "4", "Kyle Rittenhouse" },
{ "5", "Brian Laundrie" },
{ "6", "Mega Millions" },
{ "7", "AMC Stock" },
{ "8", "Stimulus Check" },
{ "9", "Georgia Senate Race" },
{ "10", "Squid Game" }
            };
            belgischePersonen = new string[,]
            {
{ "1", "Jurgen Conings" },
{ "2", "Jan Van Hecke" },
{ "3", "Eddy Demarez" },
{ "4", "Heidi De Pauw" },
{ "5", "Sihame El Kaouakibi" },
{ "6", "Gloria Monserez" },
{ "7", "Lara Switten" },
{ "8", "Kaat Bollen" },
{ "9", "Danny Verbiest" },
{ "10", "Barbara Mertens" }
            };
        }

        private void TrendsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (TrendsComboBox.SelectedIndex != -1)
            {
                bool isItemAlAanwezig = false;
                ComboBoxItem geselecteerdItem = (ComboBoxItem)TrendsComboBox.SelectedItem;
                foreach (ListBoxItem item in TrendsListBox.Items)
                {
                    if (geselecteerdItem.Content.ToString().Equals(item.Content.ToString()))
                    {
                        isItemAlAanwezig = true;
                    }
                }
                if (!isItemAlAanwezig)
                {
                    ListBoxItem item = new ListBoxItem();
                    item.Content = geselecteerdItem.Content;
                    TrendsListBox.Items.Add(item);
                }
                UpdateTrends();
                TrendsComboBox.SelectedIndex = -1;
            }
        }
        private void UpdateTrends()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (ListBoxItem item in TrendsListBox.Items)
            {
                string key = item.Content.ToString();
                stringBuilder.AppendLine(key.ToUpper());
                string[,] trend = trendsDictionary[key];
                for (int i = 0; i < GetTop(); i++)
                {
                    stringBuilder.AppendLine($"{trend[i, 0]} {trend[i, 1]}");
                }
                stringBuilder.AppendLine();
            }
            TrendsTextBox.Text = stringBuilder.ToString();
        }

        private void TrendsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (TrendsListBox.SelectedIndex !=-1)
            {
                // TrendsListBox.Items.RemoveAt(TrendsListBox.SelectedIndex);
                TrendsListBox.Items.Remove(TrendsListBox.SelectedItem);
            }
            UpdateTrends();
        }

        private int GetTop()
        {
            if (Top3RadioButton.IsChecked == true)
            {
                return 3;
            }
            else if (Top5RadioButton.IsChecked == true)
            {
                return 5;
            }
            else
            {
                return 10;
            }
        }
        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            UpdateTrends();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.C)
            {
                Clear();
            }
            else if (e.Key == Key.Q)
            {
                Close();
            }
        }

        private void Clear()
        {
            Top3RadioButton.IsChecked = true;
            TrendsListBox.Items.Clear();
            TrendsTextBox.Clear();
        }

        private void ClearMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        private void CloseMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MessageBoxResult antwoord = MessageBox.Show("Ben je zeker?", "Afsluiten", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
            if (antwoord!=MessageBoxResult.Yes)
            {
                e.Cancel = true;
            }
        }
    }
}
