﻿using System;

namespace VeelGebruikteKlassesKL
{
    class Program
    {
        static void Main(string[] args)
        {
            double pi = Math.PI;
            Console.WriteLine($"Dit is het getal {pi}");

            double getal = 3.44499;

            Console.WriteLine(getal);
            Console.WriteLine(Math.Ceiling(getal));
            Console.WriteLine(Math.Floor(getal));

            Console.WriteLine(Math.Max(100,5));
            Console.WriteLine(Math.Min(100, 5));

            if("test" == "test")
            {

            }
            if ("test".Equals("test"))
            {

            }
            // 0 1 2
            // aap
            Console.WriteLine("aap".IndexOf('a'));

            string tekst = "input";
            if (String.IsNullOrEmpty(tekst))
            {
                
            }
            Console.WriteLine(tekst.Substring(2,2));

            // Hier wordt een constructor: we geven de info manueel mee
            DateTime vandaag = new DateTime(2021, 10, 28);

            // Roep een eigenschap op van de DateTime klasse
            DateTime nu = DateTime.Now;
            Console.WriteLine(nu.ToShortTimeString());
            Console.WriteLine(vandaag.ToShortDateString());

            TimeSpan Duur = new TimeSpan(1, 0, 0);

            DateTime morgen = vandaag.AddDays(1);
            Console.WriteLine(morgen.Date);
            Console.WriteLine(morgen.Year);
            Console.WriteLine(morgen.Month);

        }
    }
}
