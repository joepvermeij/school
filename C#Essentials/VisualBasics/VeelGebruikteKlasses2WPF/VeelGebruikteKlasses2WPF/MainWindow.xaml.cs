﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VeelGebruikteKlasses2WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string mijnTekst;
        private StringBuilder tekstBouwer;
        public MainWindow()
        {
            InitializeComponent();
            mijnTekst = "Lege lijst";
            tekstBouwer = new StringBuilder();
            tekstBouwer.Append("blik bruine bonen");
            LijstTextBox.Text = tekstBouwer.ToString();

            char karakter = '4';
            if (char.IsNumber(karakter))
            {
                MessageBox.Show("het is een getal");
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            mijnTekst = mijnTekst + inputTextBox.Text + "\n";
            string hoeveelheid = Interaction.InputBox("Hoeveel", "Hoeveel Producten wil je toevegen", "1", 200, 200);
            tekstBouwer.AppendLine(inputTextBox.Text + $" {hoeveelheid}");
            tekstBouwer.Replace('a', '4');
            LijstTextBox.Text = tekstBouwer.ToString();
        }

        private void Wissen_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult antwoord = MessageBox.Show("ben je zeker?", "wissen", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (MessageBoxResult.Yes == antwoord)
            {
                tekstBouwer.Clear();
                LijstTextBox.Text = tekstBouwer.ToString();
            }
        }
    }
}
