﻿using System;

namespace ConsoleIteratie3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input hoogte van driehoek");
            int i = Convert.ToInt32(Console.ReadLine());
            int k = i;
            int a = 1;
            while(i>0)
            {
                for(int b = k; b >= i; b--)
                {
                    Console.Write(a+" ");
                    a++;
                } 
                i--;
                Console.WriteLine("");
            }
        }
    }
}
