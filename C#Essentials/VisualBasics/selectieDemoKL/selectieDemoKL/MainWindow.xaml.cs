﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace selectieDemoKL
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int getal = 0;
            string inputTekst = input.Text;
            string output = "";

            //// Parse methode
            //getal = int.Parse(inputTekst);
            
            //string voorbeeldtekst = "2.5";
            //decimal nogeengetal = decimal.Parse(voorbeeldtekst);
            //// TryParse methode
            bool ishetconverterengelukt = int.TryParse(inputTekst, out getal);
            if (ishetconverterengelukt)
            {
                input.Background = Brushes.Green;
                output = "het converteren is gelukt";

            // %levert de rest na deling op

            // int rest = getal % 2;
            
            // if -> + tab + tab
                if (getal % 2 == 0)
                {
                    output = "even";
                    if (getal>=10)
                    {
                        output = output + "en het is groter dan 10";
                    
                    }
                }
                else
                {
                    output = "oneven";
                    if (getal>=10)
                    {
                    output = output + "en het is groter dan 10";
                    }
                
                }
                input.Text = output;
            }
            else
            {
                input.Text = "NIET MOGELIJK";
            }
            
        }

        private void islekker_Click_1(object sender, RoutedEventArgs e)
        {
            string voedsel = input.Text;
            string output = ";";
            switch (voedsel)
            {
                case "appel":
                    output = "lekker";
                    break;
                case "banaan":
                    output = "lekker";
                    break;
                case "citroen":
                    output = "niet lekker";
                    break;
                default:
                    output = "Dit stuk fruit ken ik niet";
                    break;

            }
            input.Text = output;
        }
    }
}
