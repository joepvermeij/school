﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace frmSparen
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Berekenen_Click(object sender, RoutedEventArgs e)
        {
            double weekGeld = Convert.ToDouble(WeekGeldTextBox.Text);
            double wekelijkseVerhoging = Convert.ToDouble(WekelijkseVerhogingTextBox.Text);
            double verhoging = wekelijkseVerhoging;
            double huidigSpaarBedrag = weekGeld;
            double gewenstSpaarBedrag = Convert.ToDouble(GewenstSpaarBedragTextBox.Text);
            int week = 0;
            while (huidigSpaarBedrag<gewenstSpaarBedrag)
            {
                week++;
                verhoging += wekelijkseVerhoging;
                huidigSpaarBedrag += verhoging;
            }
            ResultaatTextBlock.Text = $"Spaarbedrag na {week--} weken: {huidigSpaarBedrag - verhoging} euro \n\n Extra weekgeld op dat moment: {verhoging} euro \n\n Totaal spaargeld: {huidigSpaarBedrag} euro";
        }

        private void Wissen_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Afsluiten_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
