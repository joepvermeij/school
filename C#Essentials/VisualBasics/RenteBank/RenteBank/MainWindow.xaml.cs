﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RenteBank
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Wissen(object sender, RoutedEventArgs e)
        {
            Naam.Text = "";
            Kapitaal.Text = "";
            Rente.Text = "";
            Resultaat.Text = "";
        }

        private void Afsluiten(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Berekenen(object sender, RoutedEventArgs e)
        {
            if (IsGroterdannul())
            {
                Kapitaal.Background = new SolidColorBrush(Colors.LimeGreen);
            }
            
        }
        private bool IsGroterdannul()
        {
            return Convert.ToInt32(Kapitaal.Text) > 0;            
        }
    }
}
