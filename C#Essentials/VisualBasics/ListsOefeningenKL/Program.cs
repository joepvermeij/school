﻿using System;
using System.Collections.Generic;

namespace ListsOefeningenKL
{
    class Program
    {
        static void Main(string[] args)
        {
            
        }
        // oef 1
        private static int ListSum(List<int> list)
        {
            int sum = 0;
            for (int i = 0; i < list.Count; i++)
            {
                sum += list[i];
            }
            Console.WriteLine(sum);
            return sum;
        }
        // oef 2
        private static void ConcatEach(List<string> textList, string text)
        {
            for (int i = 0; i < textList.Count; i++)
            {
                textList[i] = textList[i] + text;
                Console.WriteLine(textList[i]);
            }
        }
        // oef 3
        private static int GrootsteElement(List<int> elementen)
        {
            int GrootsteGetal = int.MinValue;
            for (int i = 0; i < elementen.Count; i++)
            {
                if (elementen[i]>GrootsteGetal)
                {
                    GrootsteGetal = elementen[i];
                }
            }
            return GrootsteGetal;
        }
        // oef 4
        private static int AantalElementen(List<string> elementen, string element)
        {
            int aantalElementen = 0;
            for (int i = 0; i < elementen.Count; i++)
            {
                if (elementen[i] == element)
                {
                    aantalElementen++;
                }
            }
            return aantalElementen;
        }
        // oef 5
        private static string WillekeurigeElement(List<string> elementen)
        {
            Random randomElement = new Random();
            int indexrandomElement = randomElement.Next(elementen.Count);
            return elementen[indexrandomElement];
        }
    }
}
