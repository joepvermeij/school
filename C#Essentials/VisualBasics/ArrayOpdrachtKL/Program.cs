﻿using System;

namespace ArrayOpdrachtKL
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] weekDagen = new string[5] { "Maandag", "Dinsdag", "Woensdag", "Donderdag", "Vrijdag" };
            Console.WriteLine("Hoeveel uur per weekdag werk je aan c#?");
            Console.WriteLine(weekDagen[0]);
            int urenMaandag = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(weekDagen[1]);
            int urenDinsdag = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(weekDagen[2]);
            int urenWoensdag = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(weekDagen[3]);
            int urenDonderdag = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(weekDagen[4]);
            int urenVrijdag = Convert.ToInt32(Console.ReadLine());
            int[] urenPerWeekDag = new int[5] { urenMaandag, urenDinsdag, urenWoensdag, urenDonderdag, urenVrijdag };
            for (int i = 0; i < weekDagen.Length; i++)
            {
                Console.WriteLine($"op {weekDagen[i]} studeer {urenPerWeekDag[i]}");
            }
            Console.WriteLine(Gemiddelde(urenPerWeekDag));
        }
        private static double Gemiddelde(int[] Getallen)
        {
            double totaleUren = 0;
            double GemiddeldeAantal;
            for (int i = 0; i < Getallen.Length; i++)
            {
                totaleUren += Getallen[i];
            }
            GemiddeldeAantal = totaleUren / Getallen.Length;
            return GemiddeldeAantal;
        }
    }
}
